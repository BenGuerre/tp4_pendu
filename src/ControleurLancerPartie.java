import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import java.util.Optional;
import java.util.Set;
import java.util.HashSet;
import java.sql.Array;
import java.util.List;
import javafx.scene.control.Alert;

/**
 * Contrôleur à activer lorsque l'on clique sur le bouton rejouer ou Lancer une partie
 */
public class ControleurLancerPartie implements EventHandler<ActionEvent> {
    /**
     * modèle du jeu
     */
    private MotMystere modelePendu;
    /**
     * vue du jeu
     **/
    private Pendu vuePendu;

    /**
     * @param modelePendu modèle du jeu
     * @param p vue du jeu
     */
    public ControleurLancerPartie(MotMystere modelePendu, Pendu vuePendu) {
        this.modelePendu = modelePendu;
        this.vuePendu = vuePendu;
    }

    /**
     * L'action consiste à recommencer une partie. Il faut vérifier qu'il n'y a pas une partie en cours
     * @param actionEvent l'événement action
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        Button button = (Button) (actionEvent.getSource());
        //Si on lcique su bouton nouveau mot
        if( button.getText().contains("Nouveau")){
        //On regarde si on clique sur le bouton pour relancer une partie
             // on lance la fenêtre popup et on attends la réponse
            Optional<ButtonType> reponse = this.vuePendu.popUpPartieEnCours().showAndWait(); // on lance la fenêtre popup et on attends la réponse
            // si la réponse est oui
                if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
                    //rendre le bouton désactivé
                    vuePendu.getButtonMaison().setDisable(true);
                    System.out.println("a");
                    // on reset le chrono
                    vuePendu.getChrono().resetTime();
                    vuePendu.modeJeu();
                    Clavier clavierVue = vuePendu.getClavier();
                    List<Button> LButton = clavierVue.getClavier();
                    clavierVue.activeTout();
                }
                
        }
        else if( button.getText().contains("Lancer")){
            //On regarde si on clique sur le bouton pour relancer une partie
                 // on lance la fenêtre popup et on attends la réponse
                        //rendre le bouton désactivé
                    vuePendu.getButtonMaison().setDisable(true);
                    // on reset le chrono
                    vuePendu.getChrono().resetTime();
                    vuePendu.modeJeu();
                    Clavier clavierVue = vuePendu.getClavier();
                    List<Button> LButton = clavierVue.getClavier();
                    clavierVue.activeTout();
                    }
        vuePendu.getButtonMaison().setDisable(false);
        vuePendu.modeJeu();
        vuePendu.getChrono().start();
        vuePendu.majAffichage();
    }

}
