import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Circle ;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Génère la vue d'un clavier et associe le contrôleur aux touches
 * le choix ici est d'un faire un héritié d'un TilePane
 */
public class Clavier extends GridPane{
    /**
     * il est conseillé de stocker les touches dans un ArrayList
     */
    private List<Button> clavier;
    private double tailleLigne;
    private double tailleTouche;

    /**
     * constructeur du clavier
     * @param touches une chaine de caractères qui contient les lettres à mettre sur les touches
     * @param actionTouches le contrôleur des touches
     * @param tailleLigne nombre de touches par ligne
     */
    public Clavier(String touches, EventHandler<ActionEvent> actionTouches) {
        this.tailleLigne = 8;
        this.clavier = new ArrayList<Button>();

        for (int i = 0; i < touches.length(); i++){
            Button bouton = new Button(String.valueOf(touches.charAt(i)));
            bouton.setMinWidth(35);
            bouton.setMinHeight(35);
            bouton.setShape(new Circle(1.5));
            bouton.setMaxSize(50,50);
            bouton.setMinSize(50,50);
            this.clavier.add(bouton);
        }

        int ligne = 0;
        int colonne = 0;
        for (Button bouton : this.clavier){
            this.add(bouton, colonne, ligne);
            colonne += 1;
            if (colonne > this.tailleLigne){
                colonne = 0;
                ligne += 1;
                
            }
            
        }

        for (Button bouton : this.clavier){
            bouton.setOnAction(actionTouches);
        }
        this.setVgap(10);
        this.setHgap(10);
    }
    /**
     * Permet de récuperer une liste de touche du clavier
     * @return une liste des lettres du clavier
     */
    public List<Button> getClavier(){
        return this.clavier;
    }

    public double getTailleLigne() {
        return this.tailleLigne;
    }

    public double getTailleTouche() {
        return this.tailleTouche;
    }

    /**
     * desactive toutes les touches du clavier
     */
    public void desactiveTout(){
        for (Button bouton : this.clavier){bouton.setDisable(true);}
    }

    /**
     * active toutes les touches du clavier
     */
    public void activeTout(){
        for (Button bouton : this.clavier){bouton.setDisable(false);}
    }

    /**
     * permet de désactiver certaines touches du clavier (et active les autres)
     * @param touchesDesactivees une chaine de caractères contenant la liste des touches désactivées
     */
    public void desactiveTouches(Set<String> touchesDesactivees){
        for (Button bouton : this.clavier){
            if (touchesDesactivees.contains(bouton.getText())){
                bouton.cancelButtonProperty();
            }
        }
    }
    public void ActivesTouches(Set<String> touchesDesactivees){
        for (Button bouton : this.clavier){
            if (touchesDesactivees.contains(bouton.getText())){
                bouton.setDisable(false);
            }
        }
    }
}
