import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TitledPane;
import javafx.scene.text.TextAlignment;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import java.util.List;
import java.util.Arrays;
import java.io.File;
import java.util.ArrayList;


/**
 * Vue du jeu du pendu
 */
public class Pendu extends Application {
    /**
     * modèle du jeu
     **/
    private MotMystere modelePendu;

    /**
     * Liste qui contient les images du jeu
     */
    private ArrayList<Image> lesImages;

    /**
     * permet de compter à quelle image nous somme
     */ 
    private int compteur;

    /**
     * Liste qui contient les noms des niveaux
     */    
    public List<String> niveaux;

    // les différents contrôles qui seront mis à jour ou consultés pour l'affichage
    /**
     * le dessin du pendu
     */
    private ImageView dessin;

    /**
     * le mot à trouver avec les lettres déjà trouvé
     */
    private Text motCrypte;

    /**
     * la barre de progression qui indique le nombre de tentatives
     */
    private ProgressBar pg;

    /**
     * le clavier qui sera géré par une classe à implémenter
     */
    private Clavier clavier;
    
    /**
     * le text qui indique le niveau de difficulté
     */
    private Text leNiveau;

    /**
     * le chronomètre qui sera géré par une clasee à implémenter
     */
    private Chronometre chrono;

    /**
     * le panel Central qui pourra être modifié selon le mode (accueil ou jeu)
     */
    private BorderPane panelCentral;

    /**
     * le bouton Paramètre / Engrenage
     */
    private Button boutonParametres;

    /**
     * le bouton Accueil / Maison
     */    
    private Button boutonMaison;

    /**
     * le bouton qui permet de lancer ou relancer une partie
     */ 
    private Button bJouer;

    /**
     * initialise les attributs (créer le modèle, charge les images, crée le chrono ...)
     */
    @Override
    public void init() {
        this.modelePendu = new MotMystere("/usr/share/dict/american-english", 3, 10, MotMystere.FACILE, 10);
        this.lesImages = new ArrayList<Image>(Arrays.asList(
            new Image("pendu10.png", 500, 500, false, false),
            new Image("pendu9.png", 500, 500, false, false),
            new Image("pendu8.png", 500, 500, false, false),
            new Image("pendu7.png", 500, 500, false, false),
            new Image("pendu6.png", 500, 500, false, false),
            new Image("pendu5.png", 500, 500, false, false),
            new Image("pendu4.png", 500, 500, false, false),
            new Image("pendu3.png", 500, 500, false, false),
            new Image("pendu2.png", 500, 500, false, false),
            new Image("pendu1.png", 500, 500, false, false),
            new Image("pendu0.png", 500, 500, false, false)
        ));

        String alphabet  = "ABCDEFGHIJKLMNOPQRSTUVWXYZ-";
        this.clavier = new Clavier(alphabet, new ControleurLettres(this.modelePendu, this));
        this.panelCentral = new BorderPane();
        this.pg = new ProgressBar(1 - (100 * (double) this.modelePendu.getNbLettresRestantes() / (double) this.modelePendu.getMotATrouve().length()) / 100);
        this.chrono = new Chronometre();
        this.dessin = new ImageView();
        this.dessin.setImage(this.lesImages.get(this.modelePendu.getNbErreursMax()));
        // A terminer d'implementer
    }

    /**
     * @return  le graphe de scène de la vue à partir de methodes précédantes
     */
    private Scene laScene(){
        BorderPane fenetre = new BorderPane();
        fenetre.setTop(this.titre());
        fenetre.setCenter(this.panelCentral);
        return new Scene(fenetre, 800, 1000);
    }

    /**
     * @return le panel contenant le titre du jeu
     */
    private Pane titre(){
        // A implementer          
        Pane banniere = new Pane();
        return banniere;
    }

    // /**
     // * @return le panel du chronomètre
     // */
    // private TitledPane leChrono(){
        // A implementer
        // TitledPane res = new TitledPane();
        // return res;
    // }

    // /**
     // * @return la fenêtre de jeu avec le mot crypté, l'image, la barre
     // *         de progression et le clavier
     // */
    // private Pane fenetreJeu(){
        // A implementer
        // Pane res = new Pane();
        // return res;
    // }

    // /**
     // * @return la fenêtre d'accueil sur laquelle on peut choisir les paramètres de jeu
     // */
    // private Pane fenetreAccueil(){
        // A implementer    
        // Pane res = new Pane();
        // return res;
    // }

    // /**
    //  * charge les images à afficher en fonction des erreurs
    //  * @param repertoire répertoire où se trouvent les images
    //  */
    // private void chargerImages(String repertoire){
    //     for (int i=0; i<this.modelePendu.getNbErreursMax()+1; i++){
    //         File file = new File(repertoire+"/pendu"+i+".png");
    //         System.out.println(file.toURI().toString());
    //         this.lesImages.add(new Image(file.toURI().toString()));
    //     }
    // }
    /*
    * Créer du menu en haut
    * @return le haut de la page
    */
    public BorderPane menuTop(){
        Text titre = new Text("Jeu du Pendu");
        BorderPane top = new BorderPane();
        Tooltip tooltip_parametre=new Tooltip("Changer les paramètres du jeu");
        Tooltip tooltip_maison=new Tooltip("Retourner à l'accueil");




        //zone des trois boutons
        HBox zoneBtn = new HBox();
        Button boutonHelp = new  Button(null, new ImageView(new Image("info.png",30,30,false,false)));
        boutonMaison = new Button(null, new ImageView(new Image("home.png", 30, 30, false, false)));
        boutonParametres = new Button(null, new ImageView(new Image("parametres.png",30,30,false,false)));
        zoneBtn.getChildren().addAll(boutonMaison, boutonParametres,boutonHelp);

        boutonParametres.setTooltip(tooltip_parametre);
        boutonMaison.setTooltip(tooltip_maison);

        // Le style
        titre.setFont(Font.font("Arial", FontWeight.BOLD, 32));
        
        //Place les eléments
        top.setLeft(titre);
        top.setRight(zoneBtn);
        top.setBackground(new Background(new BackgroundFill(Color.LAVENDER, null, null)));

        //Le placement 
        top.setPadding(new Insets(15));
        BorderPane.setMargin(top, new Insets(0, 0, 10, 0));
        
        //action butons
        boutonMaison.setOnAction(new RetourAccueil(modelePendu, this));
        boutonHelp.setOnAction(new ControleurInfos(this));
        return top;
    }
    /*
    * Créer le centre de lapage de jeu lors du jeu
    */
    public VBox center(){
        // Création des Box
        VBox centre = new VBox();
        VBox boutons = new VBox(10);
        // Le bouton pour lancer une partie
        Button lancerpartie = new Button("Lancer une partie");

        // Creation du togglepane
        TitledPane tp = new TitledPane();
        ToggleGroup group = new ToggleGroup();
        tp.setText("Niveau de diff");
        tp.setCollapsible(false);

        // Création des boutons
        RadioButton facile = new RadioButton("Facile");
        RadioButton moyen = new RadioButton("Moyen");
        RadioButton difficile = new RadioButton("Difficile");
        RadioButton expert = new RadioButton("Expert");

        // mise des boutons en mode toggle 
        facile.setToggleGroup(group);
        moyen.setToggleGroup(group);
        difficile.setToggleGroup(group);
        expert.setToggleGroup(group);   

        // Cocher le bouton facile par défaut
        facile.setSelected(true);  
        
        // Ajouter les boutons dans la box
        boutons.getChildren().addAll(facile,moyen,difficile,expert);

        // Ajouter la box des boutons dans le togglepane
        tp.setContent(boutons);

        // Le style
        centre.setPadding(new Insets(15));
        tp.setPadding(new Insets(10,0,0,0));
        
        // Ajout final dans la box principale
        centre.getChildren().addAll(lancerpartie, tp);
        
        //Event
        lancerpartie.setOnAction(new ControleurLancerPartie(modelePendu, this));
        facile.setOnAction(new ControleurNiveau(modelePendu));
        moyen.setOnAction(new ControleurNiveau(modelePendu));
        difficile.setOnAction(new ControleurNiveau(modelePendu));
        expert.setOnAction(new ControleurNiveau(modelePendu));

        return centre;
    }
    /**
     * Permet d'afficher la partie centrale du jeu 
     * @return le visuel la partie centrale du jeu
     */
    public VBox centreJeu(){
        //Le mot + l'image + progression
        VBox centre = new VBox();
            // ajoute l'image
            //récupère le mot crypte et change ça police
        this.motCrypte = new Text(this.modelePendu.getMotCrypte()); 
        this.motCrypte.setFont(Font.font("Arial", FontWeight.BOLD, 32));
        
        this.clavier.setAlignment(Pos.CENTER);
        
        centre.getChildren().addAll(motCrypte, this.dessin, pg, this.clavier);
        //on les met au centre

        VBox.setMargin(clavier, new Insets(30, 0, 0, 0));
        VBox.setMargin(pg, new Insets(10, 0, 0, 0));

        centre.setAlignment(Pos.CENTER);
        return centre;
    }
    
    /**
     * Permet de creer le visuel de la droite de la partie
     * @return le visuel de la droit de la partie
     */
    public GridPane droiteJeu(){
        // création de la gridpane
        GridPane droite = new GridPane();
        Label margin = new Label(" ");
        droite.setVgap(30);
        droite.setVgap(30);
        droite.setAlignment(Pos.BASELINE_RIGHT);

        
        // création du titre
        Label titre = new Label("Niveau " + modelePendu.getNomNiveau());
        titre.setFont(Font.font("Arial",FontWeight.BOLD,35));
        
        // création du chrono et de son interface
        TitledPane tp = new TitledPane();
        Button b = new Button("Nouveau mot");
        b.setOnAction(new ControleurLancerPartie(modelePendu, this));
        tp.setText("Chrono");
        tp.setCollapsible(false);
        tp.setContent(this.chrono);

        // ajout dans le gridpane 
        droite.add(titre,0,0);
        droite.add(tp,0,1);
        droite.add(b,0,2);
        droite.add(margin,1,0);

        return droite;

    }
    /**
     * Permet obtenir le mot crypte
     * @return le mot crypte
     */
    public Text getMotCrypte() {
        return motCrypte;
    }

    /**
     * Set le mot crypté    
     * @param motCrypte 
     */
   public void setMotCrypte(Text motCrypte) {
       this.motCrypte = motCrypte;
   }
    /**
     * Permet d'afficher l'accueil
     */
    public void modeAccueil(){
        this.panelCentral.setTop(this.menuTop());
        this.panelCentral.setCenter(this.center());
        this.panelCentral.setRight(null);
    }
    
    /** Permet d'afficher le jeu */
    public void modeJeu(){
        // On créer un nouveau mot de la bonne difficulutée
        this.modelePendu.setMotATrouver();
        this.panelCentral.setTop(this.menuTop());
        this.panelCentral.setCenter(this.centreJeu());
        this.panelCentral.setRight(this.droiteJeu());
    }

    public void modeParametres(){
        // A implémenter
    }

    /** lance une partie */
    public void lancePartie(){
        
    }

    /**
     * raffraichit l'affichage selon les données du modèle
     */
    public void majAffichage(){
        System.out.println(this.modelePendu.getMotATrouve());
        this.motCrypte.setText(this.modelePendu.getMotCrypte());
        this.dessin.setImage(this.lesImages.get(this.modelePendu.getNbErreursRestants()));
        
        // Le pourcentage de progression de la progresse bar
        double pourcentage = 1 - (100 * (double) this.modelePendu.getNbLettresRestantes() / (double) this.modelePendu.getMotATrouve().length()) / 100;
        this.pg.setProgress(pourcentage);
    }

    /**
     * accesseur du chronomètre (pour les controleur du jeu)
     * @return le chronomètre du jeu
     */
    public Chronometre getChrono(){
        return this.chrono; 
    }
    
    /**
     * Permet d'obtenir le bouton home
     * @return le bouton home
     */
    public Button getButtonMaison() {
        return this.boutonMaison;
    }
    /**
     * Permet d'afficher une pop up pour voir si on veux arreter la partie en cour
     * @return une pop up
     */
    public Alert popUpPartieEnCours(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"La partie est en cours!\n Etes-vous sûr de l'interrompre ?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Attention");
        return alert;
    }
    /**
     * Permet d'afficher une pop up avec les règles du jeu
     * @return une pop up
     */
    public Alert popUpReglesDuJeu(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION,
        "- Appuie sur les lettres pour trouver le mot final\n"+
        "\n- Les lettres grisé sont des lettres déjà utilisé\n"+ 
        "\n - Tu as seulement 10 essaies, quand le pendu est totalement dessiné, c'est perdu !\n"+ 
        "\n - Si tu bloque, n'hésite pas à cliquer sur nouveau mot pour changer de mot\n"+
        "\n - Si tu veux changer les paramètres du jeu n'hésite pas à cliquez sur le bouton paramètre disponible à l'acceuil",
        ButtonType.OK);
        alert.setHeight(450);
        return alert;
    }


    /**
     * Permet de dire que la partie est gagnée et ralnce une partie
     * @return une pop up
     */
    public Alert popUpMessageGagne(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "WIN en " + this.chrono.getText());
        this.chrono.stop();        
        return alert;
    }
    
    /**
     * Permet de dire que la partie est perdue et de relancer une partie
     * @return une pop up
     */
    public Alert popUpMessagePerdu(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "LOSE en " + this.chrono.getText());
        this.chrono.stop(); 
        return alert;
    }

    /**
     * créer le graphe de scène et lance le jeu
     * @param stage la fenêtre principale
     */
    @Override
    public void start(Stage stage) {
        stage.setTitle("IUTEAM'S - La plateforme de jeux de l'IUTO");
        stage.setScene(this.laScene());
        this.modeAccueil();
        // this.modeJeu();
        stage.show();
    }


    /**
     * Permet de prendre le clavier
     */
    public Clavier getClavier() {
        return clavier;
    }

    /**
     * Programme principal
     * @param args inutilisé
     */
    public static void main(String[] args) {
        launch(args);
    }    
}
