import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;

/**
 * Controleur du clavier
 */
public class ControleurLettres implements EventHandler<ActionEvent> {

    /**
     * modèle du jeu
     */
    private MotMystere modelePendu;
    /**
     * vue du jeu
     */
    private Pendu vuePendu;

    /**
     * @param modelePendu modèle du jeu
     * @param vuePendu vue du jeu
     */
    ControleurLettres(MotMystere modelePendu, Pendu vuePendu){
        this.modelePendu = modelePendu;
        this.vuePendu = vuePendu;

    }

    /**
     * Actions à effectuer lors du clic sur une touche du clavier
     * Il faut donc: Essayer la lettre, mettre à jour l'affichage et vérifier si la partie est finie
     * @param actionEvent l'événement
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        Button button = (Button) (actionEvent.getSource());
        //Recupération de la lette
        char lettre = button.getText().charAt(0);
        //désactivation du bouton
        button.setDisable(true);
        //On regarde si la lettre est dans le mot 
        this.modelePendu.essaiLettre(lettre);
        // maj de l'affichage
        this.vuePendu.majAffichage();
        // regarde si gagné
        if( this.modelePendu.gagne()){
            this.vuePendu.popUpMessageGagne().showAndWait();
            this.vuePendu.getClavier().desactiveTout();
        }
        //regarde si perdu
        else if( this.modelePendu.perdu()){
            this.vuePendu.popUpMessagePerdu().showAndWait();
            this.vuePendu.getClavier().desactiveTout();
            // this.vuePendu.setMotCrypte(this.modelePendu.getMotATrouve());
        }
    }
}
